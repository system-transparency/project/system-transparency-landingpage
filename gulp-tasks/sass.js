const {dest, src} = require('gulp');
const cleanCSS = require('gulp-clean-css');
const sassProcessor = require('gulp-sass')(require('sass'));

// We want to be using canonical Sass, rather than node-sass
sassProcessor.compiler = require('sass');


// The main Sass method grabs all root Sass files,
// processes them, then sends them to the output calculator
const sass = () => {
  return src('./src/scss/*.scss')
    .pipe(sassProcessor().on('error', sassProcessor.logError))
    .pipe(
      cleanCSS()
    )
    .pipe(dest('./page/css', {sourceMaps: true}))
};

module.exports = sass;