var gulp = require('gulp');
var svgstore = require('gulp-svgstore');
var svgmin = require('gulp-svgmin');
var path = require('path');


// The main Sass method grabs all root Sass files,
// processes them, then sends them to the output calculator
const svgicons = () => {
  return gulp.src('./src/icons/*.svg')
    .pipe(svgmin(function (file) {
      var prefix = path.basename(file.relative, path.extname(file.relative));
      return {
        plugins: [{
            removeViewBox: false
          },{
          cleanupIDs: {
            prefix: prefix + '-',
            minify: true
          }
        }]
      }
    }))
    .pipe(svgstore())
    .pipe(gulp.dest('page/icons'));
};

module.exports = svgicons;