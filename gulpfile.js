const {parallel, watch, task} = require('gulp');
const browserSync = require('browser-sync').create();

// Pull in each task
const sass = require('./gulp-tasks/sass.js');
const svgicons = require('./gulp-tasks/svg-store.js');
const fonts = require('./gulp-tasks/fonts.js');


// Start local server and run gulp watch tasks.
// Then reload browser when CSS oder HTML files change

const browsersync = () => {
  browserSync.init({
    server: {
      baseDir: "./page/"
    }
  });

  watch('./src/scss/**/*.scss', {ignoreInitial: true}, sass);
  watch('./src/icons/**/*.svg', {ignoreInitial: true}, svgicons);
  watch("./page/*.html").on('change', browserSync.reload);
  watch("./page/css/*.css").on('change', browserSync.reload);
};

// Set each directory and contents that we want to watch and
// assign the relevant task. `ignoreInitial` set to true will
// prevent the task being run when we run `gulp watch`, but it
// will run when a file changes.
const watcher = () => {
  watch('./src/scss/**/*.scss', {ignoreInitial: true}, sass);
  watch('./src/icons/**/*.svg', {ignoreInitial: true}, svgicons);
};

// The default (if someone just runs `gulp`) is to run each task in parrallel
exports.default = parallel(sass, svgicons);

// This is our watcher task that instructs gulp to watch directories and
// act accordingly
exports.watch = watcher;

exports.icon = svgicons;

exports.serve = browsersync;

exports.getfonts = fonts;