#! /bin/sh

podman build -t system-transparency-landingpage containerfiles

podman run --volume "$PWD:/usr/app" --publish 127.0.0.1:3000:3000 --rm -it -e START_SERVE=yes system-transparency-landingpage
