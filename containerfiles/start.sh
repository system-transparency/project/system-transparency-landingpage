#! /bin/sh
set -eu

npm install # wish we could npm install in Containerfile instead but that doesn't seem to do it
npx -y gulp
npx gulp icon

# If START_SERVE is defined, start a server on 3000/tcp, useful for
# web page development.
[[ -v START_SERVE ]] && npm -y start
