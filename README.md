# System Transparency Page

## CSS Files

Gulp Task compiles SCSS from src/scss to page/css.
Type `npx gulp` to start complier.

## Icons

Place SVG Icons in src/icons/ then run `npx gulp icon` to create icon-sprite in page folder.
Use icons like this: `<svg class="icon"><use xlink:href="icons/icons.svg#icon-twitter"></use></svg>`

## Browsersync

Start Server with `npm start`
(watches for changes in scss folder and icon folder)
